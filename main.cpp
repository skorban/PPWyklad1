#include <stdio.h>
#include <string.h> //Wykorzystano bibliotekę string.h by porównać

int main() {
    printf("Hello, World! Zadanie 1 eWykładu Podstaw Programowania");

    //Subtask 1: pozwalającą otworzyć plik "plik.bin" oraz odczytać TYLKO 5 bajt z tego pliku
    printf("\nWykonuję task numer 1:");

    FILE *fileBin;
    if ((fileBin = fopen("plik.bin", "rb") ) == NULL) //Otwarcie pliku w trybie odczytu binarnego
    {
        printf("\nPlik nie istnieje, proszę sprawdzić położenie pliku");
        return(0);
    }
    fseek(fileBin, 4,0); //ustawiam piąte (4) miejsce od początku (0) w stream. Liczymy od 0 wiec piąty bajt to 4
    printf("\nPiąty bajt to: %i ", fgetc(fileBin));
    fseek(fileBin, 4,0); //ponownie ustawiam wskaźnik streamu na miejscu 5 bo fgetc przesunął go
    printf("czyli %c", fgetc(fileBin));
    fclose(fileBin); //zamykam plik, czyli sprzatam po sobie

    printf("\nZadanie wykonane.");

    //Subtask 2: utworzyć nowy plik "plik.txt" oraz zapisać do niego ciąg znaków "Mój pierwszy plik"
    printf("\nWykonuję task numer 2:");

    FILE *fileText = fopen("plik.txt", "w"); //Tworze plik "plik.txt" w trybie zapisu (nadpisuje stare dane).
    fputs("Mój pierwszy plik", fileText); //Wpisuję zadany string do pliku
    fclose(fileText); //Zamykam (zapisuje) plik

    printf("\nZadanie wykonane. Plik znajduje się w katalogu cmake-build-debug.");

    //Subtask 3: wyszukujący ciągu znaków "pies" w pliku tekstowym o wielkości 3GB
    printf("\nWykonuję task numer 3:");
    /*
     * Ponieważ nie tworzę i nie zamieszczam pliku o tej wielkści, ta kod automatycznie wskaże że plik nie istnieje.
     * Gdyby plik znajdował się we wskazanej ścieżce (katalog cmake-build-debug), nastąpiłoby wyszukiwanie wg.
     * kodu który zamioeszoczny jest poniżej.
     *
     * Dla celów testowych zamieściłem I Księgę Pana Tadeusza i niej wykonuję wyszukiwanie
     */

    char ciag[100], szukany[] = "pies";
    char* traf;
    int linia = 1;
    FILE * fileSearch;

    if ( (fileSearch = fopen("plikDuzy.txt", "r") ) == NULL )
    {
        printf("\nPlik nie istnieje, proszę sprawdzić położenie pliku"); //Manualne "exeption catch" :)
        return(0);
    }

    while (fgets(ciag, sizeof(ciag), fileSearch) != NULL)
    {
        traf = strstr(ciag, szukany);
        /*
         * Bazując tylko na bibliotece stdio.h poszukiwania byłby wysoce nieefektywne. Jeśli nie byłoby innej
         * możliwości to pewnie stworzyłbym dwie tablice char po 4 bajty, jedną działajaca jak użyty u mnie
         * ciag[100] a druga jak szukany[], a następnie porównyał jej bezporednio za pomoca zmiennej typu boolean.
         *
         * Takie rozwiazanie jest mało efektywne i po długich walkach z samym sobą zaimplementowałem string.h oraz
         * wykorzystałem funkcje strstr. Możliwe byłby też wykorzystanie funkcji strcmp aby wyszukać dokładnie słowo
         * pies a nie tak jak w tym przypadku słowa pieszo ponieważ zawiera pies.
         *
         * Aby zaprezentować działanie tej części programu, do rozwiązania załączam plik który użyłem do testów czyli
         * I Księgę (Gospodarstwo) Pana Tadeusza, która jest już umieszczona tak aby program miał do niej dostęp,
         * oraz zrzut ekranu z mojego IDE obok kodu żródłowego.
         *
         * Na koniec nadmieniam że zdaję sobie sprawę że linie są dość subiektywne, dlatego że wynikają z ustalonego
         * przez moją osobę limitu 100 znaków w ciag.
         */

        if (traf)
        {
            printf("\nZnaleziono ciąg '%s' na lini %d", szukany, linia);
        } else
        {
            linia++;
        }
    }

    printf("\nZadanie wykonane.");

    return 0;
}
