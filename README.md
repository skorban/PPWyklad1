Zapoznaj się z funkcjami z biblioteki standardowej stdio.h :

https://pl.wikibooks.org/wiki/C/Biblioteka_standardowa/Indeks_tematyczny#stdio.h

1) Zaproponuj i omów program (sekwencję komend) pozwalającą otworzyć plik "plik.bin" oraz odczytać TYLKO 5 bajt z tego pliku.

2) Zaproponuj i omów program (sekwencję komend) pozwalającą utworzyć nowy plik "plik.txt" oraz zapisać do niego ciąg znaków "Mój pierwszy plik".

3) Zaproponuj i omów program wyszukujący ciągu znaków "pies" w pliku tekstowym o wielkości 3GB

Aby poprawnie rozwiązać zadanie należy zaproponować "algorytm"/"schemat" działania programu. Opisać kolejno wykorzystane funkcje z biblioteki standardowej jak również wykorzystane argumenty.
